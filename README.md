# Bulls and Cows

This is the starting repository for the Bulls and Cows
exercise.

## How to use this repository

1. Fork the repository to your own user or organization
1. Clone _your_ copy of the repository
1. Complete the implementation of the game in _game.py_
