from random import randint

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    answer = []
    while len(answer) < 4:
        random_var = randint(0,9)
        if random_var not in answer:
            answer.append(random_var)
    return answer

        # TODO:
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers
def str2int(user_in):
    userlist = []
    for letter in user_in:
        number = int(letter)
        userlist.append(number)
    return userlist

        # TODO:
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret
def exactcount(lista,listb):
    pairs = zip(lista,listb)
    count = 0
    for a,b in pairs:
        if a ==b:
            count+=1
    return count

        # TODO:
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret
def commoncount(lista,listb):
    count = 0
    for a in lista:
        if a in listb:
            count+=1
    return count



def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    number_of_guesses = input("How many guesses do you want? ")
    print()

    secret = generate_secret()
    
    number_of_guesses = int(number_of_guesses)

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # DONE:
        # while the length of their response is not equal
        # to 4,
        # tell them they must enter a four-digit number
        # prompt them for the input, again
        while len(guess) != 4:
            print("Please enter a four-digit number")
            guess = input(prompt)
        

        # DONE:
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers

        convertedguess = str2int(guess)

        # DONE:
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret
        
        bulls = exactcount(convertedguess,secret)

        # DONE:
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret

        cows = commoncount(convertedguess,secret)

        # DONE:
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.

        if bulls == 4:
            print("Congrats! You've won!")
            break

        # DONE:
        # report the number of "bulls" (exact matches)
        print("You have ", bulls, " bulls")
        # DONE:
        # report the number of "cows" (common entries - exact matches)
        print("You have ", cows-bulls, " cows")

    # DONE:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.
    print("You lose! The answer was ", secret, ". Better luck next time!")


# Runs the game
def run():
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    play_game()
    choice = input("Do you want to play again? (y/n)")
    while choice == "y":
        play_game()
        choice = input("Do you want to play again? (y/n)")
    exit()


if __name__ == "__main__":
    run()
